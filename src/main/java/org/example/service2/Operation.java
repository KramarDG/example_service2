package org.example.service2;

import org.apache.camel.Exchange;

public class Operation {
    public void setName(CheckBlock req, Exchange exchange){
        exchange.getIn().setHeader("NAME",req.getArg0().getName());
    }

    public CheckBlockResponse prepareResponce(Exchange exchange){
        Result res = new Result();
        res.setResultCheck(exchange.getIn().getHeader("CamelSqlRowCount").toString());
        CheckBlockResponse out = new CheckBlockResponse();
        out.setReturn(res);
        return out;
    }

}
